// deps
const { app, BrowserWindow, ipcMain } = require('electron');
const fs = require('fs');
const iconv = require('iconv-lite');
const detect = require('charset-detector');
const db = require('better-sqlite3')('library.db', { memory: false });

const main = async () =>
{
	const dir_sep = '\\';

	// read settings
	const library_location = __dirname + dir_sep + 'lyricslibrary' + dir_sep;
	const fallback_encoding = '';

	// init db
	db.exec('CREATE TABLE IF NOT EXISTS library(id INTEGER PRIMARY KEY, title TEXT, file_name TEXT, first_line TEXT, chorus_line TEXT, folder_name TEXT, info TEXT, path TEXT);');

	// start app
	const win = [];
	app.on('ready', () =>
	{
		win[0] = new BrowserWindow({ width: 1280, minWidth: 1280, height: 720, minHeight: 720, webPreferences: { enableBlinkFeatures: 'OverlayScrollbars' } });
		win[0].loadFile('html/index.html');

		win[0].on('close', app.quit);
	});

	// read library
	ipcMain.on('refresh_library', async (event) =>
	{
		await db.exec('DELETE FROM library');
		const elem_list = fs.readdirSync(library_location, { encoding: 'buffer' });

		const folder_list = [];
		const get_folder_list = elem_list.map((elem) =>
		{
			return new Promise((resolve) =>
			{
				const loc = iconv.decode(elem, detect(elem)[0].charsetName);
				if(fs.lstatSync(library_location + loc + dir_sep).isDirectory()) folder_list.push(loc);
				resolve();
			});
		});
		await Promise.all(get_folder_list);
		
		const add_library = folder_list.map((folder) => // for each folder
		{
			return new Promise(async (resolve) =>
			{
				const file_list = fs.readdirSync(library_location + folder + dir_sep, { encoding: 'buffer' });
				const read_file = file_list.map((file) => // for each file
				{
					return new Promise((resolve) =>
					{
						const file_name = iconv.decode(file, detect(file)[0].charsetName);
						const ext = file_name.substr(file_name.length - 4);
						if(ext !== '.txt' && ext !== '.TXT') return resolve();
						const loc = library_location + folder + dir_sep + file_name;
						const buffer = fs.readFileSync(loc);
						const raw = iconv.decode(buffer, detect(buffer)[0].charsetName);
						const lines = raw ? raw.split('\n') : [];
						let title = lines[0].trim() ? lines[0].trim() : file_name;
						let first_line = null;
						let chorus_line = null;
						let info = null;
						for(let c in lines)
						{
							c = parseInt(c);
							if(c === lines.length - 1) continue;
							const line = lines[c].trim(); 
							if(line.startsWith('.'))
							{
								const next_line = lines[c + 1] ? lines[c + 1].trim() : '';
								let indicator = parseInt(line.substr(1));
								if(!Number.isNaN(indicator))
								{
									switch(indicator)
									{
									case 0:
										chorus_line = next_line;
										break;
									case 1: 
										first_line = next_line;
										break;
									}
								}
								else if(line.substr(1).startsWith('i')) info = line.substr(2).trim();
							}
							else if(!first_line && c !== 0) first_line = line;
						}
						db.prepare('INSERT INTO library (title, file_name, first_line, chorus_line, folder_name, info, path) VALUES (?, ?, ?, ?, ?, ?, ?)')
							.run(title, file_name, first_line, chorus_line, folder, info, loc);
						resolve();
					});
				});
				await Promise.all(read_file);
				resolve();
			});
		});

		await Promise.all(add_library);
		event.sender.send('log', 'done!');
	});

	// search
	ipcMain.on('search', (event, arg) =>
	{
		const search_str = '%' + arg + '%';
		const result = db.prepare('SELECT * FROM library WHERE LOWER(title) LIKE LOWER(?) OR LOWER(file_name) LIKE LOWER(?) OR LOWER(first_line) LIKE LOWER(?) OR LOWER(chorus_line) LIKE LOWER(?) OR LOWER(info) LIKE LOWER(?)')
			.all([search_str, search_str, search_str, search_str, search_str]);
		event.sender.send('search_result', result);
	});

	ipcMain.on('load_song', (event, arg) =>
	{
		const buffer = fs.readFileSync(arg);
		const raw = (iconv.decode(buffer, detect(buffer)[0].charsetName)).replace(/(\r\n)/g, '\n');
		const chunks = raw ? raw.split('\n\n') : [];
		const slides = [ { index: -1, lyrics: [] } ];
		for(let c in chunks)
		{
			if(parseInt(c) === 0) continue;
			const chunk = chunks[c].trim();
			const lines = chunk.split('\n');
			let i = 0;
			if(chunk.startsWith('.')) 
			{
				i++;
				slides.push({ index: parseInt(lines[0].substr(1)), lyrics: [] });
			}
			let str = '';
			while(i < lines.length)
			{
				let line = lines[i++].trim();
				if(line === '.') line = '';
				str = str + '<br>' + line;
			}
			slides[slides.length - 1].lyrics.push(str.substr(4));
		}
		event.sender.send('update_slides', slides);
	});

	ipcMain.on('start_presentation', (event) =>
	{
		console.log('sp')
		win[1] = new BrowserWindow();
		win[1].loadFile('html/presentation_window.html');
	});

	let text = '';

	ipcMain.on('update_presentation', (event, args) =>
	{
		text = args;
	});

	ipcMain.on('fetch', (event) =>
	{
		event.sender.send('update', text);
	});
};

main();